#!/bin/sh
pwd=$PWD
cd /opt/netlogo-5.3.1-64/app
# -Djava.library.path=./lib     ensure JOGL can find native libraries
# -Djava.ext.dirs=              ignore any existing JOGL installation
# -Xmx1024m                     use up to 1GB RAM (edit to increase)
# -Dfile.encoding=UTF-8         ensure Unicode characters in model files are compatible cross-platform
# -jar NetLogo.jar              specify main jar
# "$@"                          pass along any command line arguments
java \
        -Djava.library.path=./lib \
        -Djava.ext.dirs= \
        -Xmx1024m \
        -Dfile.encoding=UTF-8 \
        -jar NetLogo.jar "$@"
cd $pwd

