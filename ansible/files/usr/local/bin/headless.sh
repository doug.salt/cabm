#!/bin/sh
RAM=`free -m | grep Mem: | awk '{print $2'}`
cwd=$PWD
cd /opt/netlogo-5.3.1-64/app
java \
  -Xmx${RAM}m \
  -Dfile.encoding=UTF-8 \
  -XX:ParallelGCThreads=1 \
  -cp NetLogo.jar org.nlogo.headless.Main \
  $@
cd $pwd
