#!/bin/sh
#$ -cwd
#$ -t 1-10
#$ -pe smp 2
printf -v JOB_ID "%02d" $(expr $SGE_TASK_ID - 1)
export JAVA_HOME="/usr/lib64/java"
wd=`pwd`
cd "/home/ds42723/NetLogo-6.0.4"
xml="$wd/zombies.nlogo-setup.xml"
xpt="x-$JOB_ID"
out="$wd/x-$JOB_ID.out"
csv="$wd/x-$JOB_ID-table.csv"
"/home/ds42723/NetLogo-6.0.4/netlogo-headless.sh" --model "$wd/zombies.nlogo" --setup-file "$xml" --experiment "$xpt" --threads 2 --table "$csv" > "$out" 2>&1
            