This is the directory to set up the work area for the 2019 ESSA summer school

1. In this directory `./nlogo.py segregation.nlogo param param.csv` - This outputs the parameters that might be change to `param.csv`. In this case:
```
parameter,type,setting,minimum,maximum
%-similar-wanted,numeric,30.0,0.0,100.0
number,numeric,2000.0,500,2500
```
2. Create the setup file...
```
./nlogo.py segregation.nlogo monte param.csv 300 10 segregation-setup.xml
```

http://ccl.northwestern.edu/netlogo/models/community/Zombies%20Model
