#!/bin/bash

# Doug Salt - June 2019

help() {

    echo """
    -d | --debug - sets debug on
    -c | --number-of-cpus-per-machine
    -D | --destroy - destroys number of machines set by -m
    -h | --help gets this message and exits
    -i | --input-file - this is any file the model need to initialise. 
         There can be multiple entries for this optioin.
    -I | --instantiate - creates the actual VM instances
    -l | --local - create the scripts locally to this directory for inspection
    -L | --logs - copies back the output and error logs of all job submissions
    -m | --number-of-machines - number of machines/samples  to be  used.
          This is only useful for the -I, -D and -S options, 
          otherwise it is ignored.
    -R | --run - does the actual run
    -S | --setup - creates the scripts for the runs
    -t | --ticks - nof ticks each sample/machine is going to run
    -v | --version - prints version number and exits
"""
    exit 0
}


instantiate() {
	# Setup the cluster

	for i in $(seq 0 $(($nof_machines - 1)))
	do
		machine=netlogo-$i
		
		# If the machine is already instatiated

		if gcloud compute instances list | grep -q $machine
		then
			exit
		fi

		vm $machine &
	done

}

vm() {
	machine=$1

	gcloud compute instances create $machine \
		--preemptible \
		--image-project centos-cloud \
		--image-family centos-7 \
		--zone europe-west1-b \
		--boot-disk-size 200 

	count=0
	while ! ping -c1 -s1 $machine > /dev/null 2>&1 \
	      && [ "$count" -lt 1000 ]
	do
		echo "Waiting for the $machine to instantiate"
		sleep 1
		count=$(($count + 1))
	done

	if [ "$count" -ge 1000 ]
	then
		exit
	fi

	ansible-playbook \
		-i $machine, \
		~/cabm/ansible/slave.yml

	add_host.sh $machine

}

scripts() {
	
	if [[ $INTERACTIVE  == 1 ]] \
	&& [[ -z "$nof_machines" ]]
    then
		echo "Please enter then number of machines/samples: "
		read nof_machines
	fi

    if [[ $INTERACTIVE  == 1 ]] \
	&& [[ -z "$nof_ticks" ]]
	then
		echo "Please enter then number of ticks per run: "
		read nof_ticks
	fi

	if [[ $INTERACTIVE  == 1 ]]
	then
		echo "Parameters"
		echo "=========="
		echo
        echo "Model: $model" 
        echo "Input directory: $INPUT"
        echo "Output directory: $OUTPUT"
		echo "Number of cpus: " $nof_cpus
        echo "Number of ticks: " $nof_ticks
        echo "Number of samples/machines: " $nof_machines
		echo 
		echo "Happy with this? (Y/N)"
		read answer
		if [[ $answer != "Y" && $answer != "y" ]]
		then
			#rm -rf "$INPUT"
			#rm -rf "$OUTPUT"
			exit -1
		fi
	fi
	
    # Copy over the nlogo model to the controlling node

    time_stamp=$(date '+%Y-%m-%d_%H:%M:%S')
	if [ -x "$INPUT" ]
	then
		mv "$INPUT" "$INPUT".$time_stamp
	fi
	mkdir -p $INPUT/data
	mkdir -p $INPUT/model
	cp $model $INPUT/model
	if [ -n "$input" ]
	then
		for file in $input
		do
			cp $input $INPUT/data
		done
	fi

	if [ -x "$OUTPUT" ]
	then
		mv "$OUTPUT" "$OUTPUT".$time_stamp
	fi
	mkdir -p $OUTPUT

    CWD=$(pwd)

    cd $INPUT/model
	model=${shortname}.nlogo

    # Create the parameters file

    nlogo.py ${model} \
        param \
        ${model}-parameters.csv
	
    nlogo.py ${model} \
        montq \
        ${model}-parameters.csv \
        $nof_ticks \
        $nof_machines \
        ${model}-setup.xml \
        ${model}-qsub.sh

    cd $CWD
}


run(){
	# Run the netlogo scripts

    if [ ! -f ${INPUT}/model/${shortname}.nlogo ] 
    then
        echo $0: No model file $shortname.nlogo
        exit -1
    fi
    if [ ! -f ${INPUT}/model/${shortname}.nlogo-setup.xml ] 
    then
        echo $0: No setup file for  $shortname.nlogo
        exit -1
    fi

    script=${INPUT}/model/${shortname}.nlogo-qsub.sh
    echo "script = $script"
    if [ ! -x $script ]
    then
        echo $0: No execution script for $shortname.nlogo
        exit -1
    fi

    if [ -z "$nof_machines" ]
    then
        if [[ $INTERACTIVE == 1 ]]
        then
    		echo "Please enter then number of machines/samples: "
		    read nof_machines
        else
            echo $0: No number of samples/machines for $shortname.nlogo
            exit -1
        fi
    fi
	cd "$INPUT"/model
    if [ -x qsub ]
    then
        echo "qsub -cwd -t 1-$nof_machines $script"
        qsub -cwd -t 1-$nof_machines $script
    else
        for run_id in $(seq 1 $nof_machines)
        do
            SGE_TASK_ID=$run_id ./$(basename $script)
        done
    fi
}

logs() {
	# Copy the logs of the runs.

	mkdir -p $OUTPUT/logs
	elasticluster ssh gridengine_$nof_cpus cp *.sh.o* *.sh.e* $OUTPUT/logs
}

destroy() {

	# Destroy the cluster

	for i in $(seq 0 $(($nof_machines - 1)))
	do
		machine=netlogo-$i
		
		# If the machine is already instatiated

		if ! gcloud compute instances list | grep -q $machine
		then
			exit
		fi

		delete_host.sh $machine
		qconf -de $machine

		# Now check whether it is an execution host. If it then we are
		# going to stop the script, as you really need to uninstall sge
		# before destroying the machine. This keeps SGE nice and tidy.

		if qconf -sel | grep -q $machine
		then
            echo "Aborting: $machine remains an SGE execution host!"
			echo "Please change dir to /opt/sge on the admin host"
			echo "and run the following, following the prompts."
			echo
			echo "./inst_sge -ux -host $machine"
			exit -1
		fi

		gcloud compute instances delete $machine --quiet
	done
}


# Standard flavours obtaining using:
#       gcloud compute machine-types list

# Name          Region                  Nof   Memory
#                                         CPUS
# 1-micro         europe-west1-d          1     0.60
# g1-small        europe-west1-d          1     1.70
# n1-standard-1   europe-west1-d          1     3.75
# n1-standard-2   europe-west1-d          2     7.50
# n1-standard-4   europe-west1-d          4     15.00
# n1-standard-8   europe-west1-d          8     30.00
# n1-standard-16  europe-west1-d          16    60.00
# n1-standard-32  europe-west1-d          32    120.00
# n1-standard-64  europe-west1-d          64    240.00
# n1-highcpu-2    europe-west1-d          2     1.80
# n1-highcpu-4    europe-west1-d          4     3.60
# n1-highcpu-8    europe-west1-d          8     7.20
# n1-highcpu-16   europe-west1-d          16    14.40
# n1-highcpu-32   europe-west1-d          32    28.80
# n1-highcpu-64   europe-west1-d          64    57.60
# n1-highmem-2    europe-west1-d          2     13.00
# n1-highmem-4    europe-west1-d          4     26.00
# n1-highmem-8    europe-west1-d          8     52.00
# n1-highmem-16   europe-west1-d          16    104.00
# n1-highmem-32   europe-west1-d          32    208.00
# n1-highmem-64   europe-west1-d          64    416.00

# For now I am limiting this to the low memory machines with 1, 2, 4 and 8
# This may change subsequently

# Note that you can construct variations on these as custom machines.

# So this means the flavours we have are

# n1-standard-1
# n1-standard-2
# n1-standard-4
# n1-standard-8

nof_cpus=1
nof_machines=
nof_ticks=
input=
output=
INTERACTIVE=1
DEBUG=
VERBOSE=
DESTROY=
INSTANTIATE=
SETUP=
RUN=
LOGS=
LOCALDIR=

while true
  do
  case "$1" in
    -A | --all ) RUN=1;SETUP=1;INSTANTIATE=1;LOGS=1;shift;;
    -v | --verbose ) VERBOSE=1; shift ;;
    -b | --batch ) INTERACTIVE=;shift;;
    -d | --debug ) DEBUG=1; shift ;;
    -D | --destroy ) DESTROY=1; shift;;
    -h | --help ) help; exit 0;;
    -i | --input-file ) input="$input $2";shift 2;;
    -I | --instantiate ) INSTANTIATE=1;shift;;
    -L | --logs ) LOGS=1;shift;;
    -l | --local ) LOCALDIR=$2;shift 2;;
    -m | --number-of-machines ) nof_machines=$2; shift 2;;
    -R | --run ) RUN=1;shift;;
    -S | --setup ) SETUP=1;shift;;
    -t | --ticks ) nof_ticks=$2;shift 2;;
    -c | --number-of-cpus-per-machine ) nof_cpus=$2; shift 2;;
    -* | --* ) help; exit -1 ;;
    * ) break ;;
  esac
done

NFS_INPUT_ROOT=/mnt/netlogo/input
NFS_OUTPUT_ROOT=/mnt/netlogo/output

if [ -n "$LOCALDIR" ]
then
    mkdir -p "$LOCALDIR"/netlogo/input
    mkdir -p "$LOCALDIR"/netlogo/output

    NFS_INPUT_ROOT="$LOCALDIR"/netlogo/input
    NFS_OUTPUT_ROOT="$LOCALDIR"/netlogo/output
else
    if ! mount | grep -qs $NFS_INPUT_ROOT
    then
        echo "$0: $NFS_INPUT_ROOT isnae mounted"
        exit -1
    fi

    # This is where the output is going to written

    if ! mount | grep -qs $NFS_OUTPUT_ROOT
    then
        echo "$0: $NFS_OUTPUT_ROOT isnae mounted"
        exit -1
    fi
fi

model=$1

if [ -z "$model" ]
then
    echo "$0: no model supplied"
    exit -1
fi

if [ -z "$model" ]
then
    echo "$0: no model supplied"
    exit -1
fi

shortname=$(basename $model | sed "s/.nlogo$//")
INPUT="$NFS_INPUT_ROOT"/"$shortname"
OUTPUT="$NFS_OUTPUT_ROOT"/"$shortname"

if [ -n "$INSTANTIATE" ]
then
	instantiate
fi

if [ -n "$SETUP" ]
then
    scripts
fi

if [ -n "$RUN" ]
then
	run
fi

if [ -n "$LOGS" ]
then
	logs
fi

if [ -n  "$DESTROY" ]
then
	destroy
fi

