#!/bin/sh

sudo mkdir -p /mnt/netlogo/input 2>/dev/null
sudo mkdir -p /mnt/netlogo/output 2>/dev/null
sudo mount -t nfs  \
	essa2019:/export/netlogo/input /mnt/netlogo/input
sudo mount -t nfs \
	essa2019:/export/netlogo/output /mnt/netlogo/output
sudo chmod -R 777 /export/netlogo
