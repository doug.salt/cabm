#!/bin/sh

HG=@allhosts

if echo $1 | grep -q $(hostname -d)
then
	host=$1
else
	host=$1.$(hostname -d)
fi

if qconf -shgrp $HG | grep -q "$host" 
then
	exit 0
fi

if qconf -shgrp $HG | grep -q NONE
then
	EDITOR=ed qconf -mhgrp $HG << EDIT0
s/NONE/$host/
w
q
EDIT0
else

	EDITOR=ed qconf -mhgrp $HG << EDIT1
s/hostlist/hostlist $host/
w
q
EDIT1
fi

