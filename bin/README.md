+ README.md - this file
+ add_host.sh - adds a host to the host list without need for human
  interaction. The standard way of doing this involves and editor and human
  input.
+ count.pl - this inspects the Netlogo file for the behaviour space and work
  out the number of repetitions multiplied by the specified parameter sweep.
+ delete_host.sh - deletes a host from the host list without need for human
  interaction. The standard way of doing this involves and editor and human
  input.
+ model.sh - build the setup, instantiate, run and destroy scripts for GCE, and
  can run them altogether or bit by bit.
+ prepare.sh - sets up the NFS for sharing the data on a run (completely
  independent of the SGE nfs area).
