#!/usr/bin/env perl

use strict;
use Getopt::Long;

# Author: Doug Salt

# Date: January 2018

# Purpose: To count the number of repeated values (specified parameter sweep)
# in the nlogo XML file, for a particular experiment, mutliply them together
# with the number of rep- etitions to get the actual number of runs, and then
# add each of these totals together for each experiment.

our $VERSION = 0.1;

my $VERBOSE;
my $help;
my $version;
my $EXPERIMENT;

GetOptions( 	"experiment=s" => \$EXPERIMENT,
		"verbose|V" => \$VERBOSE,
		"version|v" => \$version,
		"help" => \$help)
or die "$0: Error in command line options";

if ($help) {
	print "blah\n";
	exit 0;
}

if ($version) {
	print "$0: Version: $VERSION\n";
	exit 0;
}

my $xml = $ARGV[0];
my $total = 0;

open my $XML, "<" . $xml || die "$0: Unable to open $xml: $!";

while (my $record = <$XML>) {
	if ($record =~ /<experiment\s+name="(.*?)"\s+repetitions="(.*?)"/) {
		my $experiment = $1;
		my $repetitions = $2;
		if (defined $VERBOSE) {
      			if ($experiment eq $EXPERIMENT || ! defined $EXPERIMENT) {
				print "Experiment = $experiment with $repetitions repetitions\n";
			}
		}
		$record = <$XML>;
    		while ($record !~ /<\/experiment>/) {
    			if ($record  =~ /<enumeratedValueSet/) {
          			$record = <$XML>;
        			my $subcount = 0;
        			while ($record !~ /<\/enumeratedValueSet>/) {
		      			$subcount ++ if ($record =~ /<value /);
          				$record = <$XML>;
        			}
				$repetitions *= $subcount;

      			}
      			$record = <$XML>;
		}	
      		if ($experiment eq $EXPERIMENT || ! defined $EXPERIMENT) {
			$total += $repetitions;
			if (defined $VERBOSE) {
				print "$experiment = $repetitions runs\n";
			}
		}
		
	}
}
if (defined $VERBOSE) {
	print "Total = $total\n";
}
else {
	print "$total\n";
}
close $XML;


