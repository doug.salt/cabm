#!/bin/bash

# This is where the model, and its data are going to be copied to before they
# are run by each of the compute nodes.

# You need to calculate the LCM of all number of experiments, and then 
# nof reps can be any factor, or multiple of that LCM

ZONE=$(gcloud compute instances list | grep $(hostname) | awk '{print $2}')
PROJECT=$(gcloud config list 2>&1 | grep ^project | awk '{print $3}')
DOMAIN=$ZONE.c.$PROJECT.internal
#DOMAIN=europe-west1-c.c.essa-summer-school-2019.internal

factors() {
    result=
    for (( i=1; i<=$1; i++ ))
    do
        if (( $(($1 % $i)) == 0 ))
        then
            result="$result $i"
        fi
    done
    echo $result
}

lcm() {
    if (( $1 < $2 ))
    then
         min=$1
    else
         min=$2
    fi
    for (( i=$min; i<=$1 * $2; i++ ))
    do
         if (( $((  $i % $1 )) == 0 && $((  $i % $2 )) == 0 ))
         then
              echo $i
	      exit
         fi
    done
    echo $(($1 * $2))

}

get_lcm() {

  experiment=$2
  if echo $2 | egrep -qs -- "--all_experiments"
  then
	experiment=".*"
  fi
	
  for reps in $(sed -n \
's/^.*<experiment  *name="'$experiment'" repetitions="\([0-9][0-9]*\)".*/\1/p' \
	"$1")
  do
	if [[ -z $lcm ]]
	then 
              lcm=$reps
        fi
	lcm=$(lcm $reps $lcm)
  done
  echo $lcm
} 

get_repetitions() {

  total=0
  experiment=$2
  if echo $2 | egrep -qs -- "--all_experiments"
  then
	experiment=".*"
  fi
  for reps in $(sed -n \
's/^.*<experiment  *name="'$experiment'" repetitions="\([0-9][0-9]*\)".*/\1/p' \
	"$1")
  do
       total=$(($total + $reps))	
  done
  echo $total
}

split() {
	reps=$(get_repetitions $model $experiment)

	if [ -n "$INTERACTIVE" ]  \
	&& [ -z "$reps_per_run" ]
	then
	    echo "Total repetions = $reps"

	    allowable=
	    for factor in $(factors $(get_lcm $model $experiment)) 
	    do
		    allowable="$allowable "$(($reps / $factor))
	    done
	    ~/cabm/bin/count.pl --verbose $model
	    echo "Repetitions per run may be: $allowable"

	    echo "Please enter the number of repetitions per run you require"
	    read reps_per_run
	    while ! echo "$allowable" | grep -qs "$reps_per_run"
	    do  
		echo "Please enter the number of repetitions per run you require"
		read reps_per_run
	    done
	fi

	# Copy over the nlogo model to the controlling node

	if [ -x "$INPUT" ]
	then
		mv "$INPUT" "$INPUT".$(date '+%Y-%m-%d')
	fi
	mkdir -p $INPUT/data
	mkdir -p $INPUT/model
	cp $model $INPUT/model
	if [ -n "$input" ]
	then
		for file in $input
		do
			cp $input $INPUT/data
		done
	fi

	if [ -x "$OUTPUT" ]
	then
		mv "$OUTPUT" "$OUTPUT".$(date '+%Y-%m-%d')
	fi
	mkdir -p $OUTPUT

	model=$INPUT/model/$(basename $model)

	ALL=
	if echo $2 | grep -qs -- "--all_experiments"
	then
		ALL=$experiment
		experiment=
	fi

	# Split up the scripts

	split_nlogo_experiment \
		--output_dir $INPUT \
		--script_output_dir $INPUT \
		--repetitions_per_run $reps_per_run \
		--create_script ~/cabm/template/netlogo.template.sh \
		--csv_output_dir $OUTPUT \
		$ALL $model $experiment


	if [[ $INTERACTIVE  == 1 ]]
	then
		echo "Parameters"
		echo "=========="
		echo
                echo "Input directory: $INPUT"
                echo "Output directory: $OUTPUT"
		echo "Number of repetitions per run: " $reps_per_run
		echo "Number of cpus: " $nof_cpus
		echo "Number of scripts: " $(ls -1 $INPUT/*.sh | wc -l)
		echo 
		echo "Happy with this? (Y/N)"
		read answer
		if [[ $answer != "Y" && $answer != "y" ]]
		then
			#rm -rf "$INPUT"
			#rm -rf "$OUTPUT"
			exit -1
		fi
	fi
		
	if [[ $INTERACTIVE  == 1 ]] \
	&& [[ -z "$nof_machines" ]]
	then
		echo "Please enter then number of machines: "
		read nof_machines
	fi
}

instantiate() {
	# Setup the cluster

	for i in $(seq 0 $(($nof_machines - 1)))
	do
		machine=$shortname-$i
		
		# If the machine is already instatiated

		if gcloud compute instances list | grep -q $machine
		then
			exit
		fi

		vm $machine
	done

}

run(){
	# Run the netlogo scripts

	cd "$INPUT"
	for script in $(ls -1 $INPUT/*_script*.sh)
	do
	   echo "qsub -cwd $script"
	   qsub $script
	   #atq monitor gridengine_$nof_cpus
	done
	#monitor_model.sh start gridengine_$nof_cpus
}

logs() {
	# Copy the logs of the runs.

	mkdir -p $OUTPUT/logs
	elasticluster ssh gridengine_$nof_cpus cp *.sh.o* *.sh.e* $OUTPUT/logs
}

destroy() {

	# Destroy the cluster

	for i in $(seq 0 $(($nof_machines - 1)))
	do
		machine=$shortname-$i
		
		# If the machine is already instatiated

		delete_host.sh $machine.$DOMAIN
		qconf -de $machine.$DOMAIN

		if  gcloud compute instances list | grep -q $machine
		then
			# Now check whether it is an execution host. If it then we are
			# going to stop the script, as you really need to uninstall sge
			# before destroying the machine. This keeps SGE nice and tidy.

			if qconf -sel | grep -q $machine
			then
				echo "Aborting: $machine remains an SGE execution host!"
				echo "Please change dir to /opt/sge on the admin host"
				echo "and run the following, following the prompts."
				echo
				echo "./inst_sge -ux -host $machine"
				exit -1
			fi

			gcloud compute instances delete $machine --zone $ZONE --quiet
		fi
	done
}

help() {

echo "

+    -d | --debug - sets debug on
+    -c | --number-of-cpus-per-machine
+    -D | --destroy - destroys number of machines set by -m
+    -e | --experiment - selects a particular experiment within a model file. If this is not supplied then all models are run in the model file.
+    -h | --help gets this message and exits
+    -i | --input-file - this is any file the model need to initialise. There can be multiple entries for this optioin.
+    -I | --instantiate - creates the actual VM instances
+    -L | --logs - copies back the output and error logs of all job submissions
+    -m | --number-of-machines - number of machines to be  used. This is only useful for the -I and -D options, otherwise it is ignored.
+    -r | --repetitions_per_run - this determines the number of jobs
+    -R | --run - does the actual run
+    -S | --setup - runs the split of the experiments
+    -v | --version - prints version number and exits

"
exit 0
}


if [[ "$1" =~  ^(-)?-h ]] 
then
    help
    exit 0
fi

NFS_INPUT_ROOT=/mnt/netlogo/input
if ! mount | grep -qs $NFS_INPUT_ROOT
then
	echo "$0: $NFS_INPUT_ROOT is nae mounted"
	exit -1
fi

# This is where the output is going to written

NFS_OUTPUT_ROOT=/mnt/netlogo/output
if ! mount | grep -qs $NFS_OUTPUT_ROOT
then
	echo "$0: $NFS_OUTPUT_ROOT is nae mounted"
	exit -1
fi



vm() {
	machine=$1

	gcloud compute instances create $machine \
		--image-project centos-cloud \
		--image-family centos-7 \
		--zone $ZONE \
		--boot-disk-size 200 

#		--preemptible
	count=0
	while ! ping -c1 -s1 $machine.$DOMAIN > /dev/null 2>&1 \
	      && [ "$count" -lt 1000 ]
	do
		echo "Waiting for the $machine to instantiate"
		sleep 1
		count=$(($count + 1))
	done

	if [ "$count" -ge 1000 ]
	then
		exit
	fi

	ansible-playbook \
		-i $machine.$DOMAIN, \
		~/cabm/ansible/slave.yml

	add_host.sh $machine

}


# Standard flavours obtaining using:
#       gcloud compute machine-types list

# Name          Region                  Nof   Memory
#                                         CPUS
# 1-micro         europe-west1-d          1     0.60
# g1-small        europe-west1-d          1     1.70
# n1-standard-1   europe-west1-d          1     3.75
# n1-standard-2   europe-west1-d          2     7.50
# n1-standard-4   europe-west1-d          4     15.00
# n1-standard-8   europe-west1-d          8     30.00
# n1-standard-16  europe-west1-d          16    60.00
# n1-standard-32  europe-west1-d          32    120.00
# n1-standard-64  europe-west1-d          64    240.00
# n1-highcpu-2    europe-west1-d          2     1.80
# n1-highcpu-4    europe-west1-d          4     3.60
# n1-highcpu-8    europe-west1-d          8     7.20
# n1-highcpu-16   europe-west1-d          16    14.40
# n1-highcpu-32   europe-west1-d          32    28.80
# n1-highcpu-64   europe-west1-d          64    57.60
# n1-highmem-2    europe-west1-d          2     13.00
# n1-highmem-4    europe-west1-d          4     26.00
# n1-highmem-8    europe-west1-d          8     52.00
# n1-highmem-16   europe-west1-d          16    104.00
# n1-highmem-32   europe-west1-d          32    208.00
# n1-highmem-64   europe-west1-d          64    416.00

# For now I am limiting this to the low memory machines with 1, 2, 4 and 8
# This may change subsequently

# Note that you can construct variations on these as custom machines.

# So this means the flavours we have are

# n1-standard-1
# n1-standard-2
# n1-standard-4
# n1-standard-8

nof_cpus=1
nof_machines=
reps_per_run=
input=
output=
experiment="--all_experiments"
INTERACTIVE=1
DEBUG=
VERBOSE=
DESTROY=
INSTANTIATE=
SETUP=
RUN=
LOGS=

while true
  do
  case "$1" in
    -A | --all ) RUN=1;SETUP=1;INSTANTIATE=1;LOGS=1;shift;;
    -v | --verbose ) VERBOSE=1; shift ;;
    -b | --batch ) INTERACTIVE=;shift;;
    -d | --debug ) DEBUG=1; shift ;;
    -D | --destroy ) DESTROY=1; shift;;
    -h | --help ) help; exit 0;;
    -i | --input-file ) input="$input $2";shift 2;;
    -I | --instantiate ) INSTANTIATE=1;shift;;
    -L | --logs ) LOGS=1;shift;;
    -m | --number-of-machines ) nof_machines=$2; shift 2;;
    -r | --repetitions_per_run ) reps_per_run=$2: shift 2;;
    -R | --run ) RUN=1;shift;;
    -S | --setup ) SETUP=1;shift;;
    -c | --number-of-cpus-per-machine ) nof_cpus=$2; shift 2;;
    -e | --experiment ) experiment=$2; shift 2;;
    -* | --* ) help; exit -1 ;;
    * ) break ;;
  esac
done

model=$1

if [ -z "$model" ]
then
    echo "$0: no model supplied"
    exit -1
fi

if [ $nof_cpus -ne 1 -a $nof_cpus -ne 2 -a $nof_cpus -ne 4 -a $nof_cpus -ne 8 ]
then
	echo "$0: Incorrect number of CPUs parameter. Must be 1, 2, 4 or 8"
	exit -1
fi


shortname=$(basename $model | sed "s/.nlogo$//")
INPUT="$NFS_INPUT_ROOT"/"$shortname"
OUTPUT="$NFS_OUTPUT_ROOT"/"$shortname"

if [ -n "$SETUP" ]
then
	split
fi

if [ -n "$INSTANTIATE" ]
then
	instantiate
fi

if [ -n "$RUN" ]
then
	run
fi

if [ -n "$LOGS" ]
then
	logs
fi

if [ -n  "$DESTROY" ]
then
	destroy
fi

