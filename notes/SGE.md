# Removing a dead execution node.

List the queues 

+ qconf -sql

First, delete the queues associated with this host

+ qconf -dq <queuenames...>

Delete the host

+ qconf -de <hostname>

Finally, delete the configuration for the host

+ qconf -dconf <hostname>

