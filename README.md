# README 

### What is this repository for? ###

* This is all the Ansible scripts and shell scripts required to get a Netlogo
  model running 
* Version 0.1

Initially we are using this to run Jim's scripts as a means of replication.
This will use:

+ Ansible
+ split_nlogo_experiment

Eventually we hope to include automatic metadata generation as well.

### How do I get set up? ###

* Summary of set up

We are setting this machine up as the SGE root. That is the machine that hosts
the scheduling and shares it out. This host will be an admin and submission
host.

This will also make use split_nlogo_experiment to split up large experiments
for running either sequentially on a smaller number of nodes or in parallel on
a larger number of nodes. Generally there should be as many scripts as there
are nodes in the cluster.

* Configuration

One time install to set the machine up.

1. `sudo yum update`
2. `sudo yum install ansible`
3. `sudo yum install git`
4. `mkdir ~/.ssh`
5. 
```
cat > ~/.ssh/config << EOD
Host *
   StrictHostKeyChecking no
   UserKnownHostsFile=/dev/null
EOD
```
6. `chmod 600 ~/.ssh/config`
7. `ssh-keygen -b 4096`
    + Hit return to all prompts and then copy the key `~/.ssh/id_rsa.pub` to
      gitlab as one of you ssh keys.
    + Also copy this key to the ssh metadata for this project, so the user from
      this machine can access every other
8. `cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_hosts`
9. `chmod 600 ~/.ssh/authorized_hosts`
10. `git clone https://gitlab.com/doug.salt/cabm.git`
11. `cd cabm/ansible`
12. `curl https://ccl.northwestern.edu/netlogo/6.1.0/NetLogo-6.1.0-64.tgz  --output ~/cabm/ansible/pkgs/NetLogo-6.1.0-64.tgz`
13. `ansible-playbook -i localhost, master.yml`

Finally you need to run:

`gcloud init`

to allow for the creation, maintenance of VMS and associated facility
(providing of course, you have an account that  has sufficent power to do so).
The machine she is ready.

* How to run tests

`qconf -sh` - will show that SGE is correctly configured and can be run from this user.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Doug Salt
* Gary Polhill
