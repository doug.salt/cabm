#!/bin/bash
mkdir ~/{job}
cd ~/{job}
cp {setup} ~/{job}
MODEL=$PWD/$(basename {model})
cp {model} ~/{job}
SETUP=$PWD/$(basename {setup})
cp {setup} ~/{job}
DATA=$(dirname {setup})/data
cp -R "$DATA"/* ~/{job}
CSV=$PWD/$(basename {csv})

headless.sh \
	--model "$MODEL" \
	--setup-file "$SETUP" \
	--table "$CSV"
sudo cp -R ~/{job} $(dirname {csv})
